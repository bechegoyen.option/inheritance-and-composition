const Animal = require("./Animal");

class Sheep extends Animal {
  sheepSound = function () {
    console.log("BEEEEEEEEEEEEEEEEEEEEEEEEE");
  };
  wool = function () {
    console.log("siiiiiiii esquilame.");
  };
}

module.exports = Sheep;
