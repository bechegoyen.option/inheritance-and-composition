const Sheep = require("./Sheep");

class SheepRobot extends Sheep {
  fly = function () {
    console.log("Puedo volar");
  };
  selfdestruct = function () {
    console.log("Woa explotar !!!");
  };
  talk = function () {
    console.log("Te wua matar !!!");
  };
}
module.exports = SheepRobot;
