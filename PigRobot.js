const Pig = require("./Pig");

class PigRobot extends Pig {
  fly = function () {
    console.log("Puedo volar");
  };
  flamethrower = function () {
    console.log("Tengo un lanzallamas y no dudare en usarlo !!!");
  };
  talk = function () {
    console.log("Te wua matar !!!");
  };
}
module.exports = PigRobot;
