const Animal = require("./Animal");

class Sheep {
  eat = Animal.eat;
  move = Animal.move;
  extraMove = function () {
    //usa función Move de la clase actual y agrega nuevo log.
    this.move();
    console.log("Oveja: Movimiento extra");
  };
  sound = function () {
    //función que ejecuta la funcion EAT de la clase y además incorpora un nuevo log: Beeeeee
    this.eat("completo mientras hablo");
    console.log("Oveja: BEEEEEEEEEEEEEEEEEEEEEEEEE");
  };
  wool = function () {
    console.log("siiiiiiii esquilame.");
  };
}

module.exports = Sheep;
