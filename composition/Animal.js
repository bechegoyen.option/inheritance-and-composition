module.exports = {
  eat: (comida) => {
    console.log("Animal: Me como un " + comida);
  },
  fly: () => {
    console.log("Animal: Puedo volaaaaar !");
  },
  move: () => {
    console.log("Animal: puedo desplazarme");
  },
  poo: () => {
    console.log("Animal: Puedo ir al baño");
  },
  haveChildren: () => {
    console.log("Animal: Me puedo aparear y tener crias");
  },
};
