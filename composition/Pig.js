const Animal = require("./Animal");

const Pig = {
  move: Animal.move,
  poo: Animal.poo,
  eat: Animal.eat,
  pigSound: () => {
    console.log("Pig: oing!!!");
  },
  pigHead: () => {
    console.log("Pig: Kaezaechancho");
  },
};

module.exports = Pig;
