const Sheep = require("./Sheep");
const RobotActions = require("./RobotActions");
const mv = new Sheep();
class SheepRobot {
  move = mv.eat;
  sound = mv.move;
  fly = function () {
    console.log("Puedo volar pero de cabeza");
  };
  selfdestruct = RobotActions.selfdestruct;
  talk = RobotActions.talk;
}
module.exports = SheepRobot;
