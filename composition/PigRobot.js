const Pig = require("./Pig");
const RobotActions = require("./RobotActions");
const PigRobot = {
  move: () => {
    //Ejecuta la funcion Move de Pig. y agrega nuevo mensaje.
    Pig.move();
    console.log("PigRobot: add puedo desplazarme.");
  },
  sound: Pig.pigSound,
  fly: RobotActions.fly,
  flamethrower: RobotActions.flamethrower,
  talk: RobotActions.talk,
};
module.exports = PigRobot;
