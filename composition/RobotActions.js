const RobotActions = {
  fly: () => {
    console.log("RobotActions: Puedo volar");
  },
  flamethrower: () => {
    console.log("RobotActions: Tengo un lanzallamas y no dudare en usarlo !!!");
  },
  talk: () => {
    console.log("RobotActions: Te wua matar !!!");
  },
  selfdestruct: () => {
    console.log("RobotActions: 3....2....1....");
  },
};
module.exports = RobotActions;
