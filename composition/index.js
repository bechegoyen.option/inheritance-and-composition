const PigRobot = require("./PigRobot");
const Sheep = require("./Sheep");
console.log("\n");
console.log("OVEJA");

//Acciones que realiza una obeja utilizando
const sheep = new Sheep();
sheep.sound();
sheep.extraMove();
console.log("\n");
console.log("CHANCHO ROBOTICO");
//Acciones que puede realizar el PigRobot en base a funciones declaradas.
PigRobot.fly();
PigRobot.move();
PigRobot.sound();
PigRobot.talk();
//Muestra error dado que no es herencia, por ende la funcion poo,
//al no ser necesaria nunca cargada.
PigRobot.poo();
