const Pig = require("./Pig");
const PigRobot = require("./PigRobot");

const myPig = new Pig();

//acciones que realiza un cerdo en base a la Herencia de la clase Animal y funciones propias.
console.log("Pig Instance: actions that pig can do.");
myPig.eat();
myPig.move();

myPig.sound();
myPig.poo();

//acciones que realiza un cerdoRobot en base a la Herencia de la clase Pig que a su vez
//hereda de Animal. Puedo acceder a todas las funciones sean o no necesarias.
console.log("Pig Robot instance: actions that Robot pig can do.");
const myPigRobot = new PigRobot();

myPigRobot.move();
myPigRobot.poo();
myPigRobot.eat();

myPigRobot.sound();
myPigRobot.flamethrower();
